<?php
/**
 * Created By: Andreas
 * Date: 2/10/14
 * Time: 11:55 PM
 */

class PHipChat
{
	const ENDPOINT = 'https://api.hipchat.com/v2/';
	const CURL_TIMEOUT = 10;

	const COLOR_YELLOW = 'yellow';
	const COLOR_RED = 'red';
	const COLOR_GREEN = 'green';
	const COLOR_PURPLE = 'purple';
	const COLOR_GRAY = 'gray';
	const COLOR_RANDOM = 'random';

	const ROOM_PRIVATE = 'private';
	const ROOM_PUBLIC = 'public';

	const WEBHOOK_ROOM_MESSAGE = 'room_message';
	const WEBHOOK_ROOM_NOTIFICATION = 'room_notification';
	const WEBHOOK_ROOM_EXIT = 'room_exit';
	const WEBHOOK_ROOM_ENTER = 'room_enter';
	const WEBHOOK_ROOM_TOPIC_CHANGE = 'room_topic_change';

	const STATUS_AWAY = 'away';
	const STATUS_CHAT = 'chat';
	const STATUS_DND = 'dnd';
	const STATUS_XA = 'xa';

	private $token, $headers, $body;

	public function __construct($token)
	{
		$this->token = $token;
	}

	/**
	 * @param $url
	 * @param string $request_type
	 * @param null $params
	 * @param bool $auth
	 */
	protected function curl_request($url, $request_type='GET', $params=null, $auth=true)
	{
		$c = curl_init();

		if(strpos($url, '?')!==false)
			$url.='&';
		else
			$url.='?';

		if($auth)
			$url.='auth_token='.$this->token;

		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, self::CURL_TIMEOUT);
		curl_setopt($c, CURLOPT_VERBOSE, 1);
		curl_setopt($c, CURLOPT_HEADER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1); //See this if there are problems verifying the peer: http://docforge.com/wiki/PHP/Curl

		if($request_type=='POST' || $request_type=='PUT' || $request_type=='DELETE')
		{
			/*$param_array = array();
			foreach($params as $key=>$val)
				$param_array[] = $key.'='.$val;

			$param_string = join($param_array, '&');*/

			$json_data = json_encode($params);

			curl_setopt($c, CURLOPT_CUSTOMREQUEST, $request_type);
			curl_setopt($c, CURLOPT_POSTFIELDS, $json_data);
			curl_setopt($c, CURLOPT_HTTPHEADER, array
			(
				'Content-Type: application/json',
				'Content-Length: '.strlen($json_data)
			));
		}

		$response = curl_exec($c);

		$header_size = curl_getinfo($c, CURLINFO_HEADER_SIZE);
		$this->headers = explode("\r\n", substr($response, 0, $header_size));
		$this->body = substr($response, $header_size);
	}

	/**
	 * @param $url
	 * @param bool $auth
	 */
	private function get($url, $auth=true)
	{
		return $this->curl_request(self::ENDPOINT.$url, 'GET', null, $auth);
	}

	/**
	 * @param $url
	 * @param $params
	 * @param bool $auth
	 */
	private function post($url, $params, $auth=true)
	{
		return $this->curl_request(self::ENDPOINT.$url, 'POST', $params, $auth);
	}

	/**
	 * @param $url
	 * @param $params
	 * @param bool $auth
	 */
	private function put($url, $params, $auth=true)
	{
		return $this->curl_request(self::ENDPOINT.$url, 'PUT', $params, $auth);
	}

	/**
	 * @param $url
	 * @param $params
	 * @param bool $auth
	 */
	private function delete($url, $params, $auth=true)
	{
		return $this->curl_request(self::ENDPOINT.$url, 'DELETE', $params, $auth);
	}

	/**
	 * @return mixed
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * @return mixed
	 */
	public function getHeaders()
	{
		return $this->headers;
	}

	/**
	 * @return mixed
	 */
	public function getStatusCode()
	{
		return explode(' ', $this->headers[0])[1];
	}

	/**
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getAllRooms()
	{
		$this->get('room');
		if($this->getStatusCode()==200)
			return json_decode($this->body, true);
		else
			throw new PHipChatException($this->getStatusCode(), 'room', null, $this->getHeaders(), $this->getBody());
	}

	/**
	 * @param $name - Name of the room
	 * @param bool $allow_guest_access - Allow guest access or not
	 * @param null $owner_user_id - Owner user id. Can be user id or email address
	 * @param bool $private - accessible by other users or not?
	 * @return associative array of the json object of the body of the response
	 * @throws PHipChatException
	 */
	public function createRoom($name, $allow_guest_access=false, $owner_user_id=null, $private=false)
	{
		$url = 'room';
		$params = array
		(
			'name'=>$name,
			'allow_guest_access'=>$allow_guest_access,
			'owner_user_id'=>$owner_user_id?:'null',
			'privacy'=>$private?PHipChat::ROOM_PRIVATE:PHipChat::ROOM_PUBLIC
		);

		$this->post($url, $params);

		if($this->getStatusCode() == 201)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());
			return false;
		}
	}

	/**
	 * @param $room_id Room id - Can be the name or the ID
	 * @param $name
	 * @param $allow_guest_access
	 * @param $owner_user_id
	 * @param $private
	 * @param $topic
	 * @param bool $archived
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function updateRoom($room_id, $name, $allow_guest_access, $owner_user_id, $private, $topic, $archived=false)
	{
		$url = 'room/'.rawurlencode($room_id);
		$params = array
		(
			'name'=>substr($name, 0, 50),
			'privacy'=>$private?PHipChat::ROOM_PRIVATE:PHipChat::ROOM_PUBLIC,
			'is_archived'=>$archived,
			'is_guest_accessible'=>$allow_guest_access,
			'topic'=>$topic,
			'owner'=>array('id'=>$owner_user_id?:'null')
		);

		$this->put($url, $params);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());
			return false;
		}
	}

	/**
	 * @param $room_id
	 * @param $message
	 * @param string $color
	 * @param bool $notify
	 * @param string $message_format
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function sendNotification($room_id, $message, $color=PHipChat::COLOR_YELLOW, $notify=false, $message_format='html')
	{
		$url = 'room/'.rawurlencode($room_id).'/notification';
		$params = array
		(
			'color'=>$color,
			'message'=>htmlentities($message),
			'notify'=>$notify,
			'message_format'=>$message_format
		);
		$this->post($url, $params);

		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());
			return false;
		}
	}

	/**
	 * @param $room_id - Room name - Can be the name of the room or the ID
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getRoom($room_id)
	{
		$url = 'room/'.rawurlencode($room_id);

		$this->get($url);
		if($this->getStatusCode()==200)
			return json_decode($this->body, true);
		else
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());
	}

	/**
	 * @param $room_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function deleteRoom($room_id)
	{
		$url = 'room/'.rawurlencode($room_id);
		$this->delete($url, null);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());
	}


	/**
	 * @param $room_id
	 * @param $webhook_url
	 * @param $event
	 * @param $name
	 * @param string $pattern
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function createWebHook($room_id, $webhook_url, $event, $name, $pattern='')
	{
		$url = 'room/'.rawurlencode($room_id).'/webhook';
		$params = array
		(
			'url'=>$webhook_url,
			'event'=>$event,
			'pattern'=>$pattern,
			'name'=>$name
		);

		$this->post($url, $params);
		if($this->getStatusCode()==201)
			return json_decode($this->getBody(), true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param int $start
	 * @param int $max_results
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getAllWebHooks($room_id, $start=0, $max_results=100)
	{
		$url = 'room/'.rawurlencode($room_id).'/webhook?start-index='.intval($start).'&max-results='.intval($max_results);

		$this->get($url);

		if($this->getStatusCode()==200)
			return json_decode($this->getBody(), true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param int $start
	 * @param int $max_results
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getAllMembers($room_id, $start=0, $max_results=100)
	{
		$url = 'room/'.rawurlencode($room_id).'/member?start-index='.intval($start).'&max-results='.intval($max_results);

		$this->get($url);

		if($this->getStatusCode()==200)
			return json_decode($this->getBody(), true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param $topic
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function setTopic($room_id, $topic)
	{
		$url = 'room/'.rawurlencode($room_id).'/topic';
		$params = array
		(
			'topic'=>substr($topic, 0, 250)
		);

		$this->put($url, $params);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param $user_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function addMember($room_id, $user_id)
	{
		$url = 'room/'.rawurlencode($room_id).'/member/'.rawurlencode($user_id);

		$this->put($url, null);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param $user_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function removeMember($room_id, $user_id)
	{
		$url = 'room/'.rawurlencode($room_id).'/member/'.rawurlencode($user_id);

		$this->delete($url, null);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param $webhook_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function deleteWebHook($room_id, $webhook_id)
	{
		$url = 'room/'.rawurlencode($room_id).'/webhook/'.rawurlencode($webhook_id);

		$this->delete($url, null);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param $webhook_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getWebHook($room_id, $webhook_id)
	{
		$url = 'room/'.rawurlencode($room_id).'/webhook/'.rawurlencode($webhook_id);

		$this->get($url);
		if($this->getStatusCode()==200)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param string $date
	 * @param string $timezone
	 * @param int $start
	 * @param int $max_results
	 * @param bool $reverse
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getHistory($room_id, $date='recent', $timezone='UTC', $start=0, $max_results=100, $reverse=true)
	{
		$url = 'room/'.rawurlencode($room_id).'/history?date='.rawurlencode($date).'&timezone='.rawurlencode($timezone).'&start='.rawurlencode($start).'&max_results='.rawurlencode($max_results).'&reverse='.rawurlencode($reverse);

		$this->get($url);
		if($this->getStatusCode()==200)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $room_id
	 * @param $user_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function inviteUser($room_id, $user_id)
	{
		$url = 'room/'.rawurlencode($room_id).'/invite/'.rawurlencode($user_id);

		$this->get($url);
		if($this->getStatusCode()==200)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $user_id
	 * @param $message
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function privateMessage($user_id, $message)
	{
		$url = 'user/'.rawurlencode($user_id).'/message';

		$params = array
		(
			'message'=>$message
		);

		$this->post($url, $params);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $name
	 * @param $email
	 * @param $title
	 * @param $mention_name
	 * @param $is_group_admin
	 * @param string $timezone
	 * @param null $password
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function createUser($name, $email, $title, $mention_name, $is_group_admin, $timezone='UTC', $password=null)
	{
		$url = 'user';

		$params = array
		(
			'name'=>$name,
			'email'=>$email,
			'title'=>$title,
			'mention_name'=>$mention_name,
			'is_group_admin'=>$is_group_admin,
			'timezone'=>$timezone,
			'password'=>$password
		);

		$this->post($url, $params);
		if($this->getStatusCode()==201)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $user_id
	 * @param $name
	 * @param $email
	 * @param $title
	 * @param $status_message
	 * @param $status
	 * @param $mention_name
	 * @param $is_group_admin
	 * @param string $timezone
	 * @param null $password
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function updateUser($user_id, $name, $email, $title, $status_message, $status, $mention_name, $is_group_admin, $timezone='UTC', $password=null)
	{
		$url = 'user/'.rawurlencode($user_id);

		$params = array
		(
			'name'=>$name,
			'email'=>$email,
			'title'=>$title,
			'status'=>array
			(
				'status'=>$status_message,
				'show'=>$status
			),
			'mention_name'=>$mention_name,
			'is_group_admin'=>$is_group_admin,
			'timezone'=>$timezone,
		);

		if($password!==null)
			$params['password'] = $password;

		$this->put($url, $params);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, $params, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $user_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function deleteUser($user_id)
	{
		$url = 'user/'.rawurlencode($user_id);

		$this->delete($url, null);
		if($this->getStatusCode()==204)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}

	/**
	 * @param $user_id
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getUser($user_id)
	{
		$url = 'user/'.rawurlencode($user_id);

		$this->get($url);
		if($this->getStatusCode()==200)
			return json_decode($this->body, true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}


	/**
	 * @param int $start
	 * @param int $max_results
	 * @param bool $include_guests
	 * @param bool $include_deleted
	 * @return mixed
	 * @throws PHipChatException
	 */
	public function getAllUsers($start=0, $max_results=100, $include_guests=false, $include_deleted=false)
	{
		$url = 'user?start-index='.intval($start).'&max-results='.intval($max_results).'&include_guests='.$include_guests.'&include_deleted='.$include_deleted;

		$this->get($url);

		if($this->getStatusCode()==200)
			return json_decode($this->getBody(), true);
		else
		{
			throw new PHipChatException($this->getStatusCode(), $url, null, $this->getHeaders(), $this->getBody());

		}
	}
}

class PHipChatException extends Exception
{
	private $headers, $response, $request_parameters, $request_url;
	public function __construct($status_code, $request_url, $request_parameters, $headers, $response)
	{
		parent::__construct('PHipChat Error: '.$status_code.' while requesting URL: '.$request_url);
		$this->request_url = $request_url;
		$this->request_parameters = $request_parameters;
		$this->headers = $headers;
		$this->response = $response;
	}

	/**
	 * Headers returned by the HipChat API
	 * @return mixed
	 */
	public function getHeaders()
	{
		return $this->headers;
	}

	/**
	 * Parameters sent as part of the request
	 *
	 * @return mixed
	 */
	public function getRequestParameters()
	{
		return $this->request_parameters;
	}

	/**
	 * The response returned by the HipChat API
	 * @return mixed
	 */
	public function getResponse()
	{
		return $this->response;
	}

	public function getDetail()
	{
		return array
		(
			'url'=>$this->request_url,
			'params'=>$this->request_parameters,
			'headers'=>$this->headers,
			'body'=>$this->response
		);
	}


}